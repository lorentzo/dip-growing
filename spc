close all; clearvars; clc;

%image
img = imread('umbrealla.png');
figure, imshow(img);
img = double(img);

%conver to HSV
imgHSV = rgb2hsv(img);
%select H
img = imgHSV(:,:,1);
figure, imshow(img);

%call split
global stdtresh; 
global minsize;
global segRes; 
global index; 
global MRes;
global iter;
iter= 0;

index = 1;
segRes = zeros(size(img,1), size(img,2));
MRes = [];
minsize = 8;
stdtresh = 0.5;
%todo call split:
split(img, 1, size(img,1), 1, size(img,2));
figure, imshow(segRes);

%------------------------------------------------------------%
%split(img, xleft, xmiddle, yupper, ymiddle); %upper left
%split(img, xleft, xmiddle, ymiddle, ybot); %upper right
%split(img, xmiddle, xright, yupper, ymiddle); %lower left
%split(img, xmiddle, xright, ymiddle, ybot); %lower right

xs = 1;
xe = size(img,1);
ys = 1;
ye = size(img,2);

xleft = xs;
xmiddle = abs(xe-xs+1) / 2;
xright = xe;
yupper = ys;
ymiddle = abs(ye-ys+1) / 2;
ybot = ye;

figure;
subplot(2,2,1), imshow(img(xleft:xmiddle, yupper:ymiddle));
subplot(2,2,2), imshow(img(xleft:xmiddle, ymiddle:ybot));
subplot(2,2,3), imshow(img(xmiddle:xright, yupper:ymiddle));
subplot(2,2,4), imshow(img(xmiddle:xright, ymiddle:ybot));
%----------------------------------------------------------------%

%merge
%{
cnt = 0;
while true
    
    %cnt = cnt + 1;
    %stop
    if cnt > index
        break
    end
    
    %cut image with index
    IB = segRes == cnt;
    
    %if empty continue
    if sum(IB(:)) == 0
        cnt = cnt + 1;
        continue;
    end
    
    %upper left coordinate that is non zero
    [xf, yf] = find(IB, 1, 'first');

    %neighbours
    SE = strel('square', 3);
    mask1 = imdilate(IB, SE);
    frame = mask1 - IB;

    nonzeroneigh = nonzero(frame);
    nonzerounqneigh = unique(nonzeroneigh);

    %loop through neighbours
    connection = 0;
    for i = 1:size(nonzerounqneigh,1)
        IBS = segRes == i
        if sum(IBS(:) == 0)
            continue;
        end
        [xfs, yfs] = find(IBS, 1, 'first');

        %find avg brightness of these areas
        avg1 = 0;
        avg2 = 0;
        for a = MRes
            if a(2) == cnt
                avg1 = a(1);
            end
            if a(2) == i
                avg2 = a(1);
            end
        end

        if abs(avg2-avg1) < 5/255
            connection = 1;
            segRes(IBS) = cnt
        
    end

    if connection == 1
        cnt = cnt + 1;
    end
       
end

%}

%splitting
function split(img, xs, xe, ys, ye) 

    %global values
    global stdtresh; 
    global minsize;
    global segRes; 
    global index; 
    global MRes;
    global iter;
    
    iter = iter + 1;
    

    %mean and std for considered part of img
    subimg = img(xs:xe, ys:ye);
    substd = std(subimg(:));
    submean = mean(subimg(:));
    disp(abs(xs-xe));
    
    %figure, imshow(subimg);

    %if not uniform && subArea is not smaller than 8x8
    if (substd < stdtresh) && (abs(xs-xe) > minsize) && (abs(ye-ys) > minsize)

        %split in 4 identical parts
        xleft = xs;
        xmiddle = abs(xe-xs+1) / 2;
        xright = xe;
        yupper = ys;
        ymiddle = abs(ye-ys+1) / 2;
        ybot = ye;

        %for each call split: pass org img and sub area in global coordinates
        split(img, xleft, xmiddle, yupper, ymiddle); %upper left
        split(img, xleft, xmiddle, ymiddle, ybot); %upper right
        split(img, xmiddle, xright, yupper, ymiddle); %lower left
        split(img, xmiddle, xright, ymiddle, ybot); %lower right
    else

        %save subdivision in matrix segRes(yS:Ye,xS:xE) = index, index is
        segRes(xs:xe, ys:ye) = index;

        %save avg brightness of subdivision
        MRes = [MRes; [submean, index]];

        %subdivision index (increment for new index)
        index = index + 1;
        
        return;

    end

end

%30min, 18;30 or 18:00, 12.6.
%histogram of image, equalization, median filtration, erosion
%morphological ops on image, hough transform (how it works on image)
%steps for algorithms








