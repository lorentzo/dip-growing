clc;
clear all;
close all;

img = zeros(11,11);

%img(2,3)=1;
%img(3,4)=1;
%img(4,5)=1;
img(5,6)=1;

aMin = 1;
aMax = 15;
aStep = 1;
bMin = 1;
bMax = 15;
bStep = 1;

H = hough(img, aMin, aMax, aStep, bMin, bMax, bStep);

figure(1);
subplot(1,2,1), imshow(img);
subplot(1,2,2), imshow(H);

%hough transform
function H = hough(img, aMin, aMax, aStep, bMin, bMax, bStep)

  a_v = [aMin:aStep:aMax];
  b_v = [bMin:bStep:bMax];
  
  aSize = size(a_v,2);
  bSize = size(b_v,2);
  
  H = zeros(aSize, bSize);
  
  for x = [1:size(img,1)]
    for y = [1:size(img,2)]
      if img(x,y) == 1
        for ai = [1:aSize]
          b = y - a_v(ai) * x;
          [v, bb] = min(abs(b_v - b));
          if v <= 1
            H(ai,bb) = H(ai,bb) + 1;
          end
        end
      end
    end
  end
          
end

