clc; clear all; close all;

%loading image
img = imread('knee.png');

%aquireing point
figure(1), imshow(img);
[y,x] = ginput(1);
x = round(x);
y = round(y);

%img to double
img = double(img);

h = fspecial('gaussian');
img = imfilter(img, h);

%config params
tresh = 4;

%no of pixels in segmented area
nS = 0;

%average brightness value in segmented area
mV = 0;

%visited and segmented arrays
visited = zeros(size(img));
segmented = zeros(size(img));

%queue
queue = zeros(10000, 2);
fqueue = 1; %first
lqueue = size(queue, 1);
queuelen = 0;

%fist step: add element
queue(1,:) = [x,y];
fqueue = 1; lqueue = 1;
visited(x,y) = 1;
segmented(x,y) = 1;
queuelen = queuelen + 1;

figure(3);

while true
    
    if queuelen <= 0
        break
    end
    
    %pop first elem
    queue = circshift(queue, size(queue,1)-1, 1); %element we want to pop is last
    popedElem = queue(size(queue,1), :);
    queue(size(queue,1), :) = [0,0];
    queuelen = queuelen - 1;
    
    x = popedElem(1,1);
    y = popedElem(1,2);
    
    %increase pixel counter 
    nS = nS + 1;
    
    %update average value
    mV = (mV*(nS-1)+img(x,y))/nS;
    
    %determine 3x3 context
    %do not consider border
    if x == 1 || y == 1 || x == size(img,1) || y == size(img,1) || x == size(img,2) || y == size(img,2)
        continue
    end
    
    context = img(x-1:x+1, y-1:y+1);
    
    %iterate along 3x3 context
    for i = 1:3
        for j = 1:3
            
            %mapping from context coordinates to image coordiantes
            xtemp = x - (2-i);
            ytemp = y - (2-j);
            
            if i == j %do not compare same elem
                visited(xtemp, ytemp) = 1;
                continue
            end
            
            %distance between central and other in context
            %1st possibility: abs(context(i,j) - context(2,2)) < 7
            if abs(context(i,j) - mV) < 50  %distance: brightness
                 
                if visited(xtemp, ytemp) == 0 %not visited
                    
                    %belongs to object 
                    segmented(xtemp,ytemp)=1;
                    
                    %add to queue
                    queue(queuelen + 1, :) = [xtemp, ytemp];
                    queuelen = queuelen + 1;
                end
                
            end
            
            %set visited in image
            visited(xtemp, ytemp) = 1;
    
        end
        
    end
    
    %online drawing
    %Icopy(segmented>0)=255;
    %imshow(segmeted);
    %drawnow;
end

imshow(segmented);

        
    
    

